package com.lambkit.module.lms.controller;


import com.jfinal.aop.Clear;
import com.lambkit.distributed.node.NodeManager;
import com.lambkit.distributed.node.info.NodeBuilder;
import com.lambkit.web.controller.LambkitController;

public class IndexLmsController extends LambkitController {

	@Clear
	public void index() {
		//user
		if(hasUser()) {
			set("auth", getUser());
		}
		//node
		NodeBuilder nb = new NodeBuilder();
		setAttr("node", nb.resetNodeInfo(NodeManager.me().getNode()));
		int size = 0;
		if(NodeManager.me().getNodeTable()!=null) {
			setAttr("ntable", NodeManager.me().getNodeTable().getValues());
			size = NodeManager.me().getNodeTable().getNodes().size();
		}
		setAttr("ntsize", size);
		//render
		renderTemplate("index.html");
	}
	
}
