# lambkit-admin

#### 介绍
Lambkit管理平台

#### 软件架构
管理Lambkit配置，运行环境，以及监控。


#### 安装教程

1.  右键LambkitAdminApplication->Run As->Java Application启动项目
3.  访问http://1270.0.1:8080/lambkit

#### 使用说明

1.  主页http://1270.0.1:8080 /lambkit

### Lambkit管理平台

- 启动完成后，输入网址：[http://localhost:8080/lambkit/dev]，进入主页

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152134_1fcee603_136253.png "TIM截图20190219130726.png")

- 控制台
  
用于开发人员查看系统启动后，运行的Controller、Model、Handler、Plugin、模板语言标签，以及配置的Route、配置文件内容等信息。

controller信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152705_bb0b74bf_136253.png "1-2-controller.png")

model和table

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152721_b54189e9_136253.png "1-4-model.png")

plugin信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152736_108ff3d6_136253.png "1-5-plugin.png")

模板语言标签信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152744_12cf7684_136253.png "1-6-tag.png")

- 节点管理
  
用于分布式节点的管理和维护。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152821_5f3e45c0_136253.png "节点管理.png")

- 数据管理

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152843_c3ca1d37_136253.png "数据管理-主页.png")

该表格的所有接口

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152851_fadf74fa_136253.png "数据管理-接口页.png")

该表格的数据列表

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152858_dac77c41_136253.png "数据管理-列表页.png")

- 帮助文档

![输入图片说明](https://images.gitee.com/uploads/images/2019/0220/152912_c5aa78ab_136253.png "帮助文档.png")

